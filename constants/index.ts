export const JWT_ACESS_SECRET = 'your_secret_key';
export const JWT_ACESS_EXPIRESIN = '10h';

export const JWT_REFRESH_SECRET = 'your_secret_key';
export const JWT_REFRESH_EXPIRESIN = '30d';

//GRPC
export const GRPC_AUTH_SERVICE = 'localhost:5000';
export const GRPC_CORE_SERVICE = 'localhost:5001';

//REDIS 
export const REDIS_HOST = 'localhost';
export const REDIS_PORT = 6379;


//RABBITMQ
export const RABBITMQ_URL = 'amqp://localhost:5672';

